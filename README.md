### Informações importantes:

- Data ideal de entrega: 1 semana a partir da data de recebimento.
- O exercício é de execução individual.
- Linguagem para solução do exercício: PHP. 
- O candidato deverá subir sua solução em um repositório (exemplos: github, gitlab etc) e disponibilizar o link para a Kognita. 
- Os requisitos para a utilização do código-fonte devem ser informados.
- O teste não tem caráter eliminatório, apenas classificatório. Portanto, mesmo caso não consiga resolvê-lo por completo, envie a solução até onde conseguiu avançar.
- Qualquer dúvida, não hesite em entrar em contato.


### Exercício

##### Cenário: 

Entrar o CPF, nome do contribuinte, número de dependentes e renda bruta mensal de cada contribuinte. Após encerrar a entrada dos contribuintes, deve então calcular e apresentar no Front-end o valor a ser descontado de IRRF de cada contribuinte, em ordem crescente de valor de IRRF e ordem crescente de nome. Considere o valor do salário mínimo como R$ 1100,00.

Regras para cálculo do IRRF do contribuinte: 

Para cada contribuinte será concedido um desconto de 5% do valor Salário Mínimo por dependente. Os valores da alíquota para cálculo do imposto são:



| Renda líquida | Alíquota |
| ------ | ------ |
| até 2 salários mínimos | isento  |
| acima de 2 até 4 salários mínimos  | 7,5% |
| acima de 4 até 5 salários mínimos  | 15% |
| acima de 5 até 7 salários mínimos | 22,5% |
| acima de 7 salários mínimos | 27,5% |


Ps.: Renda Líquida = Renda Bruta - Descontos por Dependente - Desconto de IRRF



##### Requisitos: 

- Os Contribuintes, o desconto de IRRF, salário brunto, Renda Liquida e desconto por dependentes  devem ser persistidos em um banco de dados. 

- Desenvolver front-end simples para inserção dos dados através de um formulário e para apresentação do resultado obtido. 
Obs: Além do Nome e valor do IRRF, apresentar também os valores de Renda Bruta, Descontos por dependente e a Renda Líquida de cada funcionário.

- Não é necessário editar/excluir.







